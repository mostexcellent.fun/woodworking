PY?=python3

BASEDIR=$(CURDIR)
INPUTDIR=$(BASEDIR)/content
OUTPUTDIR=$(BASEDIR)/public

# https://stackoverflow.com/a/18137056
THIS_FILE := $(abspath $(lastword $(MAKEFILE_LIST)))
CURRENT_DIR := $(patsubst %/,%,$(dir $(THIS_FILE)))


MKDOCS?=/home/ewalstad/.virtualenvs/woodworking.mostexcellent.fun/bin/mkdocs

ARGS := $(wordlist 2, $(words $(MAKECMDGOALS)), $(MAKECMDGOALS))
# Default behavior is to try to run all commands passed to `make`.
# Convert parameters in _do-nothing_ arguments so `make` won't try
# to run them.
$(eval $(ARGS):;@:)


default: help

.PHONY: help


help:
	@echo 'FIXME: Get auto-help working'
	@echo 'Makefile woodworking.mostexcellent.fun'
	@echo '                                                                          '
	@echo 'Usage:                                                                    '
	@echo '   make devserver [PORT=8000]          serve and regenerate together      '
	@echo '   make requirements                   compile pip requirements file      '
	@echo '   make sync_requirements              compile and install requirements   '
	@echo '                                                                          '
	# FIXME: get this working:
	#@FILE=Makefile ${CURRENT_DIR}/makefile-help


devserver:
	"$(MKDOCS)" serve


devserver_dirty_reload:
	"$(MKDOCS)" serve --dirtyreload

## Compile pip requirements file
requirements:
	python -m pip install --upgrade pip pip-tools
	python -m piptools compile requirements.in


## Compile and install requirements file(s)
sync_requirements:
	@$(MAKE) -f $(THIS_FILE) requirements
	python -m piptools sync requirements.txt


## Upgrade, compile, and install requirements file(s)
upgrade_requirements:
	@$(MAKE) -f $(THIS_FILE) sync_requirements
	python -m piptools compile --upgrade


.PHONY: html help clean regenerate serve serve-global devserver publish
