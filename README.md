# Woodworking

The main pages are in the `docs` direcotry, with
[index.md](https://ewalstad.gitlab.io/2024-frames-for-lulu/)
([edit](https://gitlab.com/-/ide/project/ewalstad/2024-frames-for-lulu/edit/main/-/docs/index.md))
being the home page.

GitLab has
[an integrated development environment (IDE)](https://gitlab.com/-/ide/project/ewalstad/2024-frames-for-lulu/edit/main/-/)
where the pages can be edited in a web browser. To save your work, _commit_ your changes:

1. Click on the _Source Control_ icon in the left panel
1. Enter a brief commit message describing the changes you made.
1. Click the _Commit & Push_ button.
1. When prompted, choose _No  use the current branch "main"_.

