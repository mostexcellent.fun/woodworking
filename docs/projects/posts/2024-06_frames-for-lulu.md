---
draft: false
date:
  created: 2024-06-21
slug: frames-for-lulu
categories:
  - Milling
  - Picture frames
---

# Frames for Lulu

Here's the process I use for making picture frames.

<!-- more -->


## Milling

The wood for the frames came from a Sugar Pine in the middle of the property. It was weakened by many years of
drought and insects and broke last year during a heavy snowfall.

I cut the broken tree into log sections about 12' long. Those were milled into 1¾" thick slabs which I later
cut to dimensional sizes on the table saw. Those boards were run through the planer to get dimensional sizes.

<iframe width="965" height="543" src="https://www.youtube.com/embed/EQaogyVRiec"
    title="Milling lumber from a log with a chainsaw" 
    frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; 
    web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen>
</iframe>


## Cutting the frame boards

### Ripping the boards to ⅞" thick

The borads were of various thicknesses so the first thing I did was rip them all down to ⅞" thick.

<div class="grid cards" markdown>
- ![Rough cut boards](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_202745841.jpg){ width="300" loading=lazy }<figcaption>Rough cut boards</figcaption>
- ![Install the ripping blade](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_203044179.jpg){ width="300" loading=lazy }<figcaption>Install the ripping blade</figcaption>
- ![Ripping the board down to ¾" thick](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_203537690.jpg){ width="300" loading=lazy }<figcaption>Ripping the board down to ¾" thick</figcaption>
- ![The boards after ripping](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_203918603.jpg){ width="300" loading=lazy }<figcaption>The boards after ripping</figcaption>
</div>


### Ripping them to 2¼" wide

The boards were also differing widths, so with the ripping blade still in the saw, I cut them all to the same
width. This step also makes the long edges straight and parallel.

<div class="grid cards" markdown>
- ![Ripping to constant width](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_204746671.jpg){ width="300" loading=lazy }<figcaption>Ripping to constant width</figcaption>
- ![Ripping to constant width](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_205802261.jpg){ width="300" loading=lazy }<figcaption>Ripping to constant width</figcaption>
- ![The rough boards after ripping to width](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_210807154.jpg){ width="300" loading=lazy }<figcaption>The rough boards after ripping to width</figcaption>
</div>


### Planing the boards to ¾" thick

Planing the boards will make them all the same thickness:

<iframe width="1280" height="720" src="https://www.youtube.com/embed/36Eat4MT3sQ" 
    title="Planing rough-cut lumber to a specific thickness" frameborder="0" allow="accelerometer; autoplay; 
    clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" 
    referrerpolicy="strict-origin-when-cross-origin" allowfullscreen>
</iframe>

![The boards after planing](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_212050476.jpg){ width="300" loading=lazy }<figcaption>The boards after planing</figcaption>


### Cutting the notch for the glass, artwork and mat

A notch was then cut out of each board to accept the glass, artwork, and mat backing.

<div class="grid cards" markdown>
- ![Setting the width to ⅜"](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_212604060.jpg){ width="300" loading=lazy }<figcaption>Setting the width to ⅜"</figcaption>
- ![Setting the width to ⅜"](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_212614106.jpg){ width="300" loading=lazy }<figcaption>Setting the width to ⅜"</figcaption>
- ![Setting the depth to ⅜"](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_212636270.jpg){ width="300" loading=lazy }<figcaption>Setting the depth to ⅜"</figcaption>
- ![End-shot of the notch](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_212949456.jpg){ width="300" loading=lazy }<figcaption>End-shot of the notch</figcaption>
- ![The boards after being notched](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_213624506.jpg){ width="300" loading=lazy }<figcaption>The boards after being notched</figcaption>
</div>


### Cutting a decorative edge for the inside of the frames

The boards were then run through the router to make a decorative edge on the front of the frames.

<iframe width="1280" height="720" src="https://www.youtube.com/embed/UKMdXfY7xGk" 
    title="Using the router table to make a decorative edge for a picture frame" frameborder="0" allow="accelerometer; 
    autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
    referrerpolicy="strict-origin-when-cross-origin" allowfullscreen>
</iframe>

![So fancy!](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_214015427.jpg){ width="300" loading=lazy }<figcaption>So fancy!</figcaption>

## Making the frames

### Using a miter jig to cut the frame pieces

I used a miter jig to cut the corners of each board. The jig makes it easy to size the boards to the exact dimensions
of the artwork.

<div class="grid cards" markdown>
- ![The first 45 degree cut](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_214351448.jpg){ width="300" loading=lazy }<figcaption>The first 45 degree cut</figcaption>
- ![Cutting the frame piece to the exact length](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_214509198.jpg){ width="300" loading=lazy }<figcaption>Cutting the frame piece to the exact length</figcaption>
- ![Checking the cuts meet up as expected](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_215035017~2.jpg){ width="300" loading=lazy }<figcaption>Checking the cuts meet up as expected</figcaption>
</div>


## Assembling the frames

### Gluing the frame pieces together

The frames were assembled with glue and painter's tape was used to hold the frame's shape while the glue dried.

<div class="grid cards" markdown>
- ![One corner of the frame with painters tape to hold it together](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_215219657.jpg){ width="300" loading=lazy }<figcaption>One corner of the frame with painters tape to hold it together</figcaption>
- ![Gluing the corner](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_215526502.jpg){ width="300" loading=lazy }<figcaption>Gluing the corner</figcaption>
- ![The painters tape is strong enough](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_215756309.jpg){ width="300" loading=lazy }<figcaption>The painters tape is strong enough</figcaption>
- ![The frame is now left to dry](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_220616077~2.jpg){ width="300" loading=lazy }<figcaption>The frame is now left to dry</figcaption>
</div>

### Strengthening with splines

Splines are used as both a means of making the frames stronger as well as being visually appealing. Once the glue was
dry, I used a spline jig to cut each frame corner to accept splines. I am using a flat-top blade to get a closer fit
between the spline and the frame

<div class="grid cards" markdown>
- ![Installing the flat-top blade](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_221330443~2.jpg){ width="300" loading=lazy }<figcaption>Installing the flat-top blade</figcaption>
- ![Setting up the spline jig](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_221652183.jpg){ width="300" loading=lazy }<figcaption>Setting up the spline jig</figcaption>
- ![The jig holds the frame securely in place](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_222139823.jpg){ width="300" loading=lazy }<figcaption>The jig holds the frame securely in place</figcaption>
- ![The spline fits snugly](../../images/2024_06_frames_for_lulu/thumb_PXL_20240616_223236778.jpg){ width="300" loading=lazy }<figcaption>The spline fits snugly</figcaption>
</div>

The splines were made from Redwood, which gives a nice contrasting color and still strengthens the frame. The splines
were glued into place. Once dry, the excess spline material was removed using a hand plane.

## Finishing the frames

The frames were then sanded with 100 grit and then 220 grit sandpaper. Two coats of shellac were then applied,
sanding the high spots down with steel wool after each coat of shellac.

<div class="grid cards" markdown>
- ![Sanding](../../images/2024_06_frames_for_lulu/thumb_PXL_20240617_210022676.jpg){ width="300" loading=lazy }<figcaption>Sanding</figcaption>
- ![After the first coat of shellac](../../images/2024_06_frames_for_lulu/thumb_PXL_20240617_215401185.jpg){ width="300" loading=lazy }<figcaption>After the first coat of shellac</figcaption>
- ![Smoothing out the shellac with steel wool](../../images/2024_06_frames_for_lulu/thumb_PXL_20240618_132209184.jpg){ width="300" loading=lazy }<figcaption>Smoothing out the shellac with steel wool</figcaption>
</div>

A friend of mine suggested that I try finding used pictures at a local second-hand store as a
source for glass. They are cheaper than buying new glass and sometimes the mounting hardware can
be re-used, too. And, one less thing going to the landfill.  My local 
[Good Will](https://www.google.com/maps/place/Goodwill+Grass+Valley/@39.2384024,-121.0333731,18.96z/data=!4m6!3m5!1s0x809b71c556e7282d:0x5e1f85f5f01eb8ec!8m2!3d39.2380294!4d-121.0333862!16s%2Fg%2F11mvrd_jbm?entry=ttu)
had a lot of frames available. I paid between $3.00 and $6.00 for each one. I used a 
[basic glass-cutter](https://www.amazon.com/dp/B0838X6SSB?ref=ppx_yo2ov_dt_b_product_details&th=1)
to make the cuts, nothing fancy there. The glass needed cleaning, too.

I took the frames to a local 
[mailbox store](https://www.google.com/maps/place/Mailboxes+Plus/@39.2390846,-121.0376413,20.04z/data=!4m6!3m5!1s0x809b7066092ff5b9:0x7809bc60f9afd2f3!8m2!3d39.2388017!4d-121.0382346!16s%2Fg%2F1tdq686l?entry=ttu)
to help get them wrapped and shipped out. The lady there did a terrific job and helped me find the best shipping rates.
For two large frames and one small frame, the shipping cost was just under $100 from northern California to New York.

